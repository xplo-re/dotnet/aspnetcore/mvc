﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Reflection;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace XploRe.AspNetCore.Mvc.ModelBinding
{

    /// <summary>
    ///     Provides a helper method for <see cref="ModelMetadata" /> to retrieve a property attribute of a given type.
    /// </summary>
    public static class ModelMetadataExtensions
    {

        /// <summary>
        ///     Retrieves the attribute instance of type <typeparamref name="T" /> for the property with the name given
        ///     by the <see cref="ModelMetadata.PropertyName" /> property of the provided <see cref="ModelMetadata" />
        ///     instance.
        /// </summary>
        /// <typeparam name="T"><see cref="Attribute" /> type to lookup.</typeparam>
        /// <param name="modelMetadata">The <see cref="ModelMetadata" /> instance representing a property.</param>
        /// <returns>
        ///     Matching attribute instance of type <typeparamref name="T" /> if a metadata container type is defined
        ///     and the requested property attribute exists, otherwise <c>null</c>.
        /// </returns>
        [Pure]
        [CanBeNull]
        public static T GetPropertyAttribute<T>([NotNull] this ModelMetadata modelMetadata)
            where T : Attribute
        {
            if (modelMetadata == null) {
                throw new ArgumentNullException(nameof(modelMetadata));
            }

            if (modelMetadata.ContainerType != null) {
                var propertyName = modelMetadata.PropertyName;
                var property = modelMetadata.ContainerType.GetProperty(propertyName);

                return property?.GetCustomAttribute<T>();
            }

            return null;
        }

    }

}
