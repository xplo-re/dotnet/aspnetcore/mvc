﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Reflection;
using System.Text;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace XploRe.AspNetCore.Mvc.ModelBinding
{

    /// <inheritdoc />
    /// <summary>
    /// <para>
    ///     Automatically performs Unicode normalisation on <see cref="string" /> values that are bound by this 
    ///     <see cref="IModelBinderProvider" />.
    /// </para>
    /// <para>
    ///     Apply the <see cref="NormalizationFormAttribute" /> attribute to properties to explicitly define the desired
    ///     Unicode normalisation form. If 
    /// </para>
    /// <para>
    ///     The model binder is activated by inserting a <see cref="NormalizeStringModelBinderProvider" /> instance into
    ///     the ASP.NET MVC model binder providers list.
    /// </para>
    /// </summary>
    public class NormalizeStringModelBinderProvider : IModelBinderProvider
    {

        /// <summary>
        ///     The desired default normalisation form to apply if not specified individually per field or property via
        ///     the <see cref="NormalizationFormAttribute" /> attribute. Defaults to Unicode normalisation form C.
        /// </summary>
        public NormalizationForm NormalizationForm { get; } = NormalizationForm.FormC;

        /// <inheritdoc />
        [Pure]
        [CanBeNull]
        public IModelBinder GetBinder([NotNull] ModelBinderProviderContext context)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata?.ModelType == typeof(string)) {
                var normalizationForm = NormalizationForm;

                if (context.Metadata.ContainerType != null) {
                    var propertyName = context.Metadata.PropertyName;
                    var property = context.Metadata.ContainerType.GetProperty(propertyName);
                    var normalizationFormAttribute = property?.GetCustomAttribute<NormalizationFormAttribute>();

                    if (normalizationFormAttribute != null) {
                        if (normalizationFormAttribute.NormalizationForm == 0) {
                            // Normalisation has been explicitly disabled for the field/property.
                            return null;
                        }

                        // Use specified normalisation form.
                        normalizationForm = normalizationFormAttribute.NormalizationForm;
                    }
                }

                return new NormalizeStringModelBinder(normalizationForm);
            }

            return null;
        }

        /// <summary>
        ///     Initialises a new <see cref="NormalizeStringModelBinderProvider" /> instance using Unicode normalisation
        ///     form C as default.
        /// </summary>
        public NormalizeStringModelBinderProvider()
        {
        }

        /// <summary>
        ///     Initialises a new <see cref="NormalizeStringModelBinderProvider" /> instance with the given Unicode
        ///     normalisation form default.
        /// </summary>
        /// <param name="normalizationForm"></param>
        public NormalizeStringModelBinderProvider(NormalizationForm normalizationForm)
        {
            NormalizationForm = normalizationForm;
        }

    }

}
