﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Diagnostics;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc.ModelBinding
{

    /// <inheritdoc />
    /// <summary>
    ///     Performs default simple type binding for <see cref="T:System.String" /> models with automatic trimming.
    /// </summary>
    /// <seealso cref="T:Microsoft.AspNetCore.Mvc.ModelBinding.Binders.SimpleTypeModelBinder" />
    public class TrimStringModelBinder : IModelBinder
    {

        /// <inheritdoc />
        public Task BindModelAsync([NotNull] ModelBindingContext bindingContext)
        {
            if (bindingContext == null) {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            // We are working with a string value type.
            if (bindingContext.ValueProvider == null) {
                throw new ArgumentException(
                    $"Property {nameof(bindingContext.ValueProvider)} must not be null.",
                    nameof(bindingContext)
                );
            }

            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (valueProviderResult == ValueProviderResult.None) {
                // No value.
                return Task.CompletedTask;
            }

            if (bindingContext.ModelState == null) {
                throw new ArgumentException(
                    $"Property {nameof(bindingContext.ModelState)} must not be null.",
                    nameof(bindingContext)
                );
            }

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);

            // Postprocess value.
            // Cf. https://github.com/aspnet/Mvc/blob/760c8f38678118734399c58c2dac981ea6e47046/src/Microsoft.AspNetCore.Mvc.Core/ModelBinding/Binders/SimpleTypeModelBinder.cs
            var value = valueProviderResult.FirstValue?.Trim();
            object model;

            if (bindingContext.ModelMetadata == null) {
                throw new ArgumentException(
                    $"Property {nameof(bindingContext.ModelMetadata)} must not be null.",
                    nameof(bindingContext)
                );
            }

            if (bindingContext.ModelMetadata.ConvertEmptyStringToNull && string.IsNullOrEmpty(value)) {
                model = null;
            }
            else {
                model = value;
            }

            // Test if a null model is acceptable within the binding context and log an error if not.
            if (model == null && !bindingContext.ModelMetadata.IsReferenceOrNullableType) {
                var messageProvider = bindingContext.ModelMetadata.ModelBindingMessageProvider;

                Debug.Assert(messageProvider != null, $"{nameof(messageProvider)} != null");
                Debug.Assert(
                    messageProvider.ValueMustNotBeNullAccessor != null,
                    "{0}.{1} != null".FormatWith(
                        nameof(messageProvider),
                        nameof(messageProvider.ValueMustNotBeNullAccessor)
                    ));

                bindingContext.ModelState.TryAddModelError(
                    bindingContext.ModelName,
                    messageProvider.ValueMustNotBeNullAccessor(valueProviderResult.ToString())
                );

                return Task.CompletedTask;
            }

            bindingContext.Result = ModelBindingResult.Success(model);

            return Task.CompletedTask;
        }

    }

}
