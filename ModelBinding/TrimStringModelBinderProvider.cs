﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Reflection;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace XploRe.AspNetCore.Mvc.ModelBinding
{

    /// <inheritdoc />
    /// <summary>
    /// <para>
    ///     Automatically trims whitespace from <see cref="T:System.String" /> values that are bound by this 
    ///     <see cref="T:Microsoft.AspNetCore.Mvc.ModelBinding.IModelBinderProvider" />.
    /// </para>
    /// <para>
    ///     Apply the <see cref="T:XploRe.AspNetCore.Mvc.NoTrimAttribute" /> attribute to properties to exclude them 
    ///     from being bound by this provider.
    /// </para>
    /// <para>
    ///     The model binder is activated by inserting a <see cref="T:XploRe.AspNetCore.Mvc.ModelBinding.TrimStringModelBinderProvider" /> 
    ///     instance into the ASP.NET MVC model binder providers list.
    /// </para>
    /// </summary>
    public class TrimStringModelBinderProvider : IModelBinderProvider
    {

        /// <inheritdoc />
        [Pure]
        [CanBeNull]
        public IModelBinder GetBinder([NotNull] ModelBinderProviderContext context)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata?.ModelType == typeof(string)) {
                // Skip if model is a property with the NoTrim attribute.
                if (context.Metadata.ContainerType != null) {
                    var propertyName = context.Metadata.PropertyName;
                    var property = context.Metadata.ContainerType.GetProperty(propertyName);

                    if (property?.GetCustomAttribute<NoTrimAttribute>() != null) {
                        return null;
                    }
                }

                return new TrimStringModelBinder();
            }

            return null;
        }

    }

}
