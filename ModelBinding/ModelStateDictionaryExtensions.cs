﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace XploRe.AspNetCore.Mvc.ModelBinding
{

    /// <summary>
    ///     Adds an extension method to <see cref="ModelStateDictionary" /> objects to set a list of errors.
    /// </summary>
    public static class ModelStateDictionaryExtensions
    {

        /// <summary>
        ///     Adds a list of error descriptions with an empty key to the model state.
        /// </summary>
        /// <param name="modelState">
        ///     The <see cref="ModelStateDictionary" /> instance to add error descriptions to.
        /// </param>
        /// <param name="errors">Collection of error descriptions to add.</param>
        public static void AddModelErrors(
            [NotNull] this ModelStateDictionary modelState,
            [CanBeNull] IEnumerable<string> errors)
        {
            if (modelState == null) {
                throw new ArgumentNullException(nameof(modelState));
            }

            if (errors == null) {
                return;
            }

            foreach (var error in errors) {
                modelState.AddModelError(string.Empty, error);
            }
        }

    }

}
