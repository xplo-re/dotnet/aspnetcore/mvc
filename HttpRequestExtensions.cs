﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;


namespace XploRe.AspNetCore.Mvc
{

    /// <summary>
    ///     Extends <see cref="HttpRequest" /> with a method to determine whether the request was initiated using AJAX.
    /// </summary>
    public static class HttpRequestExtensions
    {

        /// <summary>
        ///     Verifies whether the request has been initiated via AJAX by checking whether the "X-Requested-With"
        ///     header is set to "XMLHttpRequest".
        /// </summary>
        /// <param name="request">The <see cref="HttpRequest" /> instance to verify.</param>
        /// <returns><c>true</c>, if the request was initiated by AJAX, otherwise <c>false</c>.</returns>
        [Pure]
        public static bool IsAjax([NotNull] this HttpRequest request)
        {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            if (request.Headers != null) {
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            }

            return false;
        }

    }

}
