﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq.Expressions;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using XploRe.AspNetCore.Mvc.ModelBinding;


namespace XploRe.AspNetCore.Mvc.Rendering
{

    /// <summary>
    /// Extensions for <see cref="IHtmlHelper{TModel}" /> instances to retrieve the display name of models.
    /// </summary>
    public static class HtmlHelperExtensions
    {

        /// <summary>
        ///     Retrieves the display name of a property of an enumerable member of the view model.
        /// </summary>
        /// <remarks>
        ///     The display name can be configured via the <see cref="DisplayAttribute" /> attribute. If no such
        ///     attribute is found, the name of the property is used instead, either derived from an existing property
        ///     or the provided expression.
        /// </remarks>
        /// <param name="htmlHelper">The <see cref="IHtmlHelper{TModel}" /> instance.</param>
        /// <param name="model">The enumerable member of the view model.</param>
        /// <param name="expression">An expression to be evaluated against an item in the enumerable model member.</param>
        /// <typeparam name="TModel">The type of the enumerable model member.</typeparam>
        /// <typeparam name="TModelItem">The type of items in the enumerable model member collection.</typeparam>
        /// <typeparam name="TResult">The type of the <paramref name="expression" /> result.</typeparam>
        /// <returns>
        ///     The display name of the property. If no display name could be determined, an empty string is returned
        ///     instead. 
        /// </returns>
        /// <exception cref="InvalidOperationException">
        ///     The provided <paramref name="expression" /> is invalid.
        /// </exception>
        [Pure]
        [NotNull]
        public static string DisplayNameFor<TModel, TModelItem, TResult>(
            [NotNull] this IHtmlHelper<TModel> htmlHelper,
            IEnumerable<TModelItem> model,
            [NotNull] Expression<Func<TModelItem, TResult>> expression)
        {
            if (htmlHelper == null) {
                throw new ArgumentNullException(nameof(htmlHelper));
            }

            if (expression == null) {
                throw new ArgumentNullException(nameof(expression));
            }

            var modelExplorer = GetModelExplorerFromLambdaExpression(htmlHelper, expression);

            if (modelExplorer == null) {
                throw new ArgumentException($"Invalid expression '{expression}'.", nameof(expression));
            }

            return modelExplorer.Metadata?.DisplayName ??
                   modelExplorer.Metadata?.PropertyName ??
                   GetLastExpressionComponent(expression) ??
                   string.Empty;
        }

        /// <summary>
        ///     Retrieves the short display name of a property of an enumerable member of the view model.
        /// </summary>
        /// <remarks>
        ///     The short display name can be configured via the <see cref="DisplayAttribute" /> attribute. If no such
        ///     attribute is found, the name of the property is used instead, either derived from an existing property
        ///     or the provided expression.
        /// </remarks>
        /// <param name="htmlHelper">The <see cref="IHtmlHelper{TModel}" /> instance.</param>
        /// <param name="model">The enumerable member of the view model.</param>
        /// <param name="expression">An expression to be evaluated against an item in the enumerable model member.</param>
        /// <typeparam name="TModel">The type of the enumerable model member.</typeparam>
        /// <typeparam name="TModelItem">The type of items in the enumerable model member collection.</typeparam>
        /// <typeparam name="TResult">The type of the <paramref name="expression" /> result.</typeparam>
        /// <returns>
        ///     The short display name of the property. If not short display name could be determined, an empty string
        ///     is returned instead.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        ///     The provided <paramref name="expression" /> is invalid.
        /// </exception>
        [Pure]
        [NotNull]
        public static string ShortDisplayNameFor<TModel, TModelItem, TResult>(
            [NotNull] this IHtmlHelper<TModel> htmlHelper,
            IEnumerable<TModelItem> model,
            [NotNull] Expression<Func<TModelItem, TResult>> expression)
        {
            if (htmlHelper == null) {
                throw new ArgumentNullException(nameof(htmlHelper));
            }

            if (expression == null) {
                throw new ArgumentNullException(nameof(expression));
            }

            var modelExplorer = GetModelExplorerFromLambdaExpression(htmlHelper, expression);

            if (modelExplorer == null) {
                throw new ArgumentException($"Invalid expression '{expression}'.", nameof(expression));
            }

            var displayAttribute = modelExplorer.Metadata?.GetPropertyAttribute<DisplayAttribute>();

            return displayAttribute?.ShortName ??
                   modelExplorer.Metadata?.PropertyName ??
                   GetLastExpressionComponent(expression) ??
                   string.Empty;
        }


        #region Internal Helpers

        /// <summary>
        ///     Retrieves the last dot-separated component from the string representation of the given expression.
        /// </summary>
        /// <param name="expression">Expression to get last component from.</param>
        /// <returns>
        ///     Last component of <paramref name="expression" /> or <c>null</c>, if conversion to string failed.
        /// </returns>
        [Pure]
        [CanBeNull]
        private static string GetLastExpressionComponent(LambdaExpression expression)
        {
            var expressionText = ExpressionHelper.GetExpressionText(expression);

            if (expressionText == null) {
                return null;
            }

            // Resolving the display name failed, use the expression text instead, reduced to the last component.
            var index = expressionText.LastIndexOf('.');

            // ReSharper disable once ConvertIfStatementToReturnStatement
            if (index == -1) {
                // Expression does not contain a dot separator.
                return expressionText;
            }

            return expressionText.Substring(index + 1);
        }

        /// <summary>
        ///     Retrieves the <see cref="ModelExplorer" /> from the view data of an <see cref="IHtmlHelper{TModel}" />,
        ///     based on a given expression.
        /// </summary>
        [Pure]
        private static ModelExplorer GetModelExplorerFromLambdaExpression<TModel, TModelItem, TResult>(
            [NotNull] IHtmlHelper<TModel> htmlHelper,
            Expression<Func<TModelItem, TResult>> expression)
        {
            Debug.Assert(htmlHelper != null, $"{nameof(htmlHelper)} != null");

            return ExpressionMetadataProvider.FromLambdaExpression(
                expression,
                new ViewDataDictionary<TModelItem>(htmlHelper.ViewData, model: default(TModelItem)),
                htmlHelper.MetadataProvider
            );
        }

        #endregion

    }

}
