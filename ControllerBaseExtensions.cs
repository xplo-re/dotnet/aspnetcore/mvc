﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;


namespace XploRe.AspNetCore.Mvc
{

    /// <summary>
    ///     Provides extensions methods for <see cref="ControllerBase" /> result types provided by this assembly.
    /// </summary>
    public static class ControllerBaseExtensions
    {

        /// <summary>
        ///     Creates a new <see cref="JavaScriptResult" /> response object instance from the provided
        ///     <paramref name="script" /> string.
        /// </summary>
        /// <param name="controller">The <see cref="ControllerBase" /> instance.</param>
        /// <param name="script">The JavaScript string to initialise the response with.</param>
        /// <returns>The created <see cref="JavaScriptResult" /> response object instance.</returns>
        [Pure]
        [NotNull]
        public static JavaScriptResult JavaScript([NotNull] this ControllerBase controller, string script)
        {
            if (controller == null) {
                throw new ArgumentNullException(nameof(controller));
            }

            return new JavaScriptResult(script);
        }

    }

}
