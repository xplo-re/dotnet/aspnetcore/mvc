﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using Microsoft.AspNetCore.Mvc;


namespace XploRe.AspNetCore.Mvc
{

    /// <summary>
    ///     Represents a raw JavaScript response from a controller action.
    /// </summary>
    public class JavaScriptResult : ContentResult
    {

        /// <summary>
        ///     The default content-type for JavaScript.
        /// </summary>
        public const string DefaultContentType = "application/javascript";

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.AspNetCore.Mvc.JavaScriptResult" /> response.
        /// </summary>
        public JavaScriptResult()
        {
            // Set default JavaScript content type.
            ContentType = DefaultContentType;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.AspNetCore.Mvc.JavaScriptResult" /> response with the given 
        ///     JavaScript code.
        /// </summary>
        /// <param name="script">JavaScript code response string.</param>
        public JavaScriptResult(string script) : this()
        {
            Content = script;
        }

    }

}
