﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Text;


namespace XploRe.AspNetCore.Mvc
{

    /// <inheritdoc />
    /// <summary>
    ///     Explicitly sets the Unicode normalisation form to apply during model binding.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class NormalizationFormAttribute : Attribute
    {

        /// <summary>
        ///     The configured Unicode normalisation form. If applicable, a value of 0 can be provided to explicitly skip
        ///     normalisation.
        /// </summary>
        public NormalizationForm NormalizationForm { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="NormalizationFormAttribute" /> instance with a given
        ///     <see cref="NormalizationForm" />.
        /// </summary>
        /// <param name="normalizationForm">
        ///     <para>
        ///     The desired Unicode normalisation form for the field or property.
        ///     </para>
        ///     <para>
        ///     If applicable, a value of 0 for the normalisation form can be used to explicitly skip normalisation for
        ///     the applied field or property.
        ///     </para>
        /// </param>
        public NormalizationFormAttribute(NormalizationForm normalizationForm)
        {
            NormalizationForm = normalizationForm;
        }

    }

}
