﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using XploRe.AspNetCore.Mvc.ModelBinding;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc
{

    /// <summary>
    ///     Provides extension method for <see cref="IMvcBuilder" /> instances to add model providers.
    /// </summary>
    public static class MvcBuilderExtensions
    {

        /// <summary>
        ///     Registers a new model binder provider of type <typeparamref name="T" /> at the beginning of the MVC
        ///     model binder providers list.
        /// </summary>
        /// <param name="builder">The <see cref="IMvcBuilder" /> instance to configure.</param>
        /// <typeparam name="T">Type of the model binder provider to register.</typeparam>
        /// <returns>The <see cref="IMvcBuilder" /> instance.</returns>
        [NotNull]
        public static IMvcBuilder AddModelBinderProvider<T>([NotNull] this IMvcBuilder builder)
            where T : IModelBinderProvider, new()
        {
            if (builder == null) {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Services.Configure<MvcOptions>(
                options => {
                    if (options == null) {
                        throw new RuntimeInconsistencyException(
                            $"{nameof(options)} is null"
                        );
                    }

                    if (options.ModelBinderProviders == null) {
                        throw new RuntimeInconsistencyException(
                            "{0}.{1} is null".FormatWith(
                                nameof(options),
                                nameof(options.ModelBinderProviders)
                            )
                        );
                    }

                    options.ModelBinderProviders.Insert(0, new T());
                });

            return builder;
        }

        /// <summary>
        ///     Adds a new <see cref="NormalizeStringModelBinderProvider" /> instance for Unicode normalisation form C
        ///     to the beginning of the MVC model binder providers list.
        /// </summary>
        /// <param name="builder">This <see cref="IMvcBuilder" /> instance to configure.</param>
        /// <returns>This <see cref="IMvcBuilder" /> instance.</returns>
        public static IMvcBuilder AddNormalizeStringModelBinderProvider([NotNull] this IMvcBuilder builder)
        {
            if (builder == null) {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.AddModelBinderProvider<NormalizeStringModelBinderProvider>();
        }

        /// <summary>
        ///     Adds a new <see cref="TrimStringModelBinderProvider" /> instance to the beginning of the MVC model
        ///     binder providers list.
        /// </summary>
        /// <param name="builder">This <see cref="IMvcBuilder" /> instance to configure.</param>
        /// <returns>This <see cref="IMvcBuilder" /> instance.</returns>
        public static IMvcBuilder AddTrimStringModelBinderProvider([NotNull] this IMvcBuilder builder)
        {
            if (builder == null) {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.AddModelBinderProvider<TrimStringModelBinderProvider>();
        }

    }

}
