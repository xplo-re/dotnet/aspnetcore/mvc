﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;


namespace XploRe.AspNetCore.Mvc.ViewFeatures
{

    /// <summary>
    /// <para>
    ///     Extends <see cref="ITempDataDictionary" /> with a set of storage and retrieval methods for complex data types
    ///     that are stored in serialised form.
    /// </para>
    /// <para>
    ///     In .NET Core only the in-memory temporary data storage provider supports arbitrary objects. Other storage
    ///     providers expect a well-known simple type and otherwise throw an exception. Serialising complex types using
    ///     JSON solves the issue by adding a small overhead.
    /// </para>
    /// </summary>
    public static class TempDataDictionaryExtensions
    {

        /// <summary>
        ///     Adds a single complex type value to a collection identified by <paramref name="key" /> that is stored in
        ///     a <see cref="ITempDataDictionary" /> in JSON serialised form. If the dictionary does not contain an
        ///     item with the provided <paramref name="key" />, a new list item holding the provided value is added.
        /// </summary>
        /// <param name="tempData">The <see cref="ITempDataDictionary" /> instance.</param>
        /// <param name="key">The key of the item that holds the list to add a new value to.</param>
        /// <param name="value">The value to add to the list.</param>
        /// <typeparam name="T">The type of the value and of the serialised list.</typeparam>
        public static void AddToSerializedList<T>(
            [NotNull] this ITempDataDictionary tempData,
            string key, T value)
        {
            if (tempData == null) {
                throw new ArgumentNullException(nameof(tempData));
            }

            var list = tempData.PeekSerialized<List<T>>(key);

            if (list != null) {
                list.Add(value);
            }
            else {
                list = new List<T> { value };
            }

            tempData.PutSerialized(key, list);
        }

        /// <summary>
        ///     Adds multiple complex type values to a collection identified by <paramref name="key" /> that is stored
        ///     in a <see cref="ITempDataDictionary" /> in JSON serialised form. If the dictionary does not contain an
        ///     item with the provided <paramref name="key" />, a new list item holding the provided value is added.
        /// </summary>
        /// <param name="tempData">The <see cref="ITempDataDictionary" /> instance.</param>
        /// <param name="key">The key of the item that holds the list to add new values to.</param>
        /// <param name="values">A collection of values to add to the end of the list.</param>
        /// <typeparam name="T">The type of the values and the serialised list.</typeparam>
        public static void AddRangeToList<T>(
            [NotNull] this ITempDataDictionary tempData,
            string key, IEnumerable<T> values)
        {
            if (tempData == null) {
                throw new ArgumentNullException(nameof(tempData));
            }

            var list = tempData.PeekSerialized<List<T>>(key);

            if (list != null) {
                list.AddRange(values);
            }
            else {
                list = values?.ToList();
            }

            tempData.PutSerialized(key, list);
        }

        /// <summary>
        ///     Returns a complex type value identified by <paramref name="key" /> from an <see cref="ITempDataDictionary" />
        ///     deserialised from JSON.
        /// </summary>
        /// <param name="tempData">The <see cref="ITempDataDictionary" /> instance.</param>
        /// <param name="key">The key of the item to get.</param>
        /// <typeparam name="T">The type of the value to retrieve.</typeparam>
        /// <returns>
        ///     The deserialised object instance of type <typeparamref name="T" /> or <c>null</c>, if no such item 
        ///     exists.
        /// </returns>
        [CanBeNull]
        public static T GetSerialized<T>([NotNull] this ITempDataDictionary tempData, string key) where T : class
        {
            if (tempData == null) {
                throw new ArgumentNullException(nameof(tempData));
            }

            tempData.TryGetValue(key, out var result);

            if (result != null) {
                return JsonConvert.DeserializeObject<T>((string) result);
            }

            return null;
        }

        /// <summary>
        ///     Returns a complex type value identified by <paramref name="key" /> from an <see cref="ITempDataDictionary" />
        ///     deserialised from JSON without marking the element for deletion.
        /// </summary>
        /// <param name="tempData">The <see cref="ITempDataDictionary" /> instance.</param>
        /// <param name="key">The key of the item to get.</param>
        /// <typeparam name="T">The type of the value to retrieve.</typeparam>
        /// <returns>
        ///     A deserialised object instance of type <typeparamref name="T" /> or <c>null</c>, if no such item exists.
        /// </returns>
        [Pure]
        [CanBeNull]
        public static T PeekSerialized<T>([NotNull] this ITempDataDictionary tempData, string key) where T : class
        {
            if (tempData == null) {
                throw new ArgumentNullException(nameof(tempData));
            }

            var result = tempData.Peek(key);

            if (result != null) {
                return JsonConvert.DeserializeObject<T>((string) result);
            }

            return null;
        }

        /// <summary>
        ///     Sets a complex type value for the specified <paramref name="key" /> in an <see cref="ITempDataDictionary" />
        ///     serialised as a JSON string for use with any kind of temporary storage provider.
        /// </summary>
        /// <param name="tempData">The <see cref="ITempDataDictionary" /> instance.</param>
        /// <param name="key">The key of the item to set.</param>
        /// <param name="value">The arbitrary type value to set.</param>
        /// <typeparam name="T">The type of the value to set.</typeparam>
        public static void PutSerialized<T>(
            [NotNull] this ITempDataDictionary tempData,
            string key, T value)
            where T : class
        {
            if (tempData == null) {
                throw new ArgumentNullException(nameof(tempData));
            }

            tempData[key] = JsonConvert.SerializeObject(value);
        }

    }

}
