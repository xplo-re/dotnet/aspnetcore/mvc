﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using XploRe.AspNetCore.Mvc.ModelBinding;


namespace XploRe.AspNetCore.Mvc
{

    /// <inheritdoc />
    /// <summary>
    /// <para>
    ///     Marks a model property or field as non-trimmable by automatic trim operations.
    /// </para>
    /// <para>
    ///     This is mainly used together with model binders such as <see cref="T:XploRe.AspNetCore.Mvc.ModelBinding.TrimStringModelBinderProvider" /> 
    ///     that provide automatic trimming of strings.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class NoTrimAttribute : Attribute
    {

    }

}
